/*
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"gitlab.com/hlf-mipt/basic-asset-chaincode-go/internal"
	"gitlab.com/hlf-mipt/basic-asset-chaincode-go/internal/logger"
	"gitlab.com/hlf-mipt/basic-asset-chaincode-go/internal/repository"
	"go.uber.org/fx"
)

func main() {
	fx.New(
		fx.Provide(logger.NewLogger),
		fx.Provide(internal.NewAppService),
		fx.Provide(repository.NewRepository),

		fx.Invoke(func(*internal.AppService) {}),
	).Run()
}
