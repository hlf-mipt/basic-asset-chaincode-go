package handler

import (
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/sirupsen/logrus"
	"gitlab.com/hlf-mipt/basic-asset-chaincode-go/internal/repository"
)

type SmartContract struct {
	contractapi.Contract
	log        *logrus.Entry
	repository *repository.Repository
}

func NewSmartContract(log *logrus.Entry, r *repository.Repository) *SmartContract {
	return &SmartContract{
		log:        log,
		repository: r,
	}
}
